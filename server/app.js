var express = require('express')
var mongoose = require('mongoose')
var signinModel = require('./src/models/signinModel')
var userModel = require('./src/models/userModel')
var pointFormModel = require('./src/models/pointFormModel')
var pointModel = require('./src/models/pointModel')
var interestPoints = require('./src/models/interestPointsModel')
var roles = require('./src/models/rolesModel')
var adminModel = require('./src/models/adminModel')
var pointImages = require('./src/models/pointImagesModel')
var pointTypes = require('./src/models/pointsTypeModel')
var userPlacesModel = require('./src/models/userPlacesModel')
var scoreModel = require('./src/models/scoreModel')
var reviewsModel = require('./src/models/reviewModel')
var challengesModel = require('./src/models/challengesModel')
//var visitedPlacesModel = require('./src/models/visitedPlacesModel');
var reviewFormModel = require('./src/models/reviewFormModel.js')
var userPlacesModel = require('./src/models/userPlacesModel')
var userScoresModel = require('./src/models/userScoresModel')
var userScoresModel = require('./src/models/preferredModel')
var confirmModel = require('./src/models/confirmModel')

//var userPlacesModel = require('./src/models/userPlacesModel');
//var scoreModel = require('./src/models/scoreModel');
//var reviewsModel = require('./src/models/reviewModel');
var bodyParser = require('body-parser');
//var session = require('express-session');
var cookieSession = require('cookie-session');
//var MongoStore = require('connect-mongodb-session')(sessions);
//var MongoStore = require('connect-mongo')(session);
var cors = require('cors')
var app = express()

var local_ip = "192.168.5.16"
//var local_ip = "localhost"

//app.use(cors())
//var Vue = require('vue');
//var VueCookie = require('vue-cookie');
// Tell Vue to use the plugin
//Vue.use(VueCookie);

mongoose.connect('mongodb://localhost/dbwebproject', { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false,  });
var connection = mongoose.connection;

/*app.use(session({
  store: new MongoStore({mongooseConnection: connection}),
  secret: 'secret here',
  resave: false,
  saveUninitialized: false,
  //httpOnly: false
}));*/

app.use(cookieSession({
    name:'session',
    keys: ['key1'],
    httpOnly: false,
  })
);

//Per gestire i parametri passati nel corpo della richiesta http.
app.use(bodyParser.json({limit: '50mb', extended: true}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

/* app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json()); */

app.use(express.static(__dirname+'/www'));
var path = require('path');
global.appRoot = path.resolve(__dirname);




const origins = [
  'http://127.0.0.1:3000', //node server
  'http://127.0.0.1:8080',  //vue client
  'http://localhost:3000', //node server
  'http://localhost:8080',  //vue client
  'http://' + local_ip + ':3000', //node server
  'http://' + local_ip + ':8080',  //vue client
];

app.use(cors({ 
  origin: origins,
  methods: ['GET', 'POST', 'DELETE', 'PUT'], 
  credentials: true
}));

/*
// ENABLING COR REQUESTS
app.options('*', cors()) 

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});
*/

var routes = require('./src/routes/approutes');
routes(app); 

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});

app.listen(3000, function () {
  console.log('Node API server started on port 3000!');
});