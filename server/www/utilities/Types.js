const types = {
    SUCCESS : 'success',
    ERROR : 'error',
    INFO: 'info'
}

module.exports = types;