var mongoose = require('mongoose');
var RequestResult = require("../../www/utilities/RequestResult");
var types = require("../../www/utilities/Types.js");
var Preferred = mongoose.model('userfavourites');

exports.get_preferred_of_user = function(req, res) {
    let userEmail = req.session.Email;
    Preferred.find({'Email' : userEmail}, function(err, favourites) {
        if (err) {
            res.send(err);
        }
        res.json(favourites);
    });
};

exports.toggle_preferred = function(req, res) {
    let userEmail = req.session.Email;
    let add = req.body.addInFavourite;
    let responseTitle = add ? "Added to favourite!" : "Removed from favourites";
    let responseText = add ? "This place has been correctly added in favourites!" : "This place has been correctly removed from favourites!"
    console.log(req.body);
    Preferred.findOne({'Email' : userEmail}, function(err, preferred) {
        let preferredToUpdate = new Array();
        if (err) {
            res.send(err);
        }else{ 
            let response = null;
            if(preferred === null && add){
                preferredToUpdate.push(req.body.Id);
            }else{
                preferredToUpdate = Array.from(preferred.Preferred);
                if(add){
                    preferredToUpdate.push(req.body.Id);
                }else{
                    let t = new Array();
                    for(let i = 0 ; i < preferredToUpdate.length; i++){
                        if(preferredToUpdate[i] !== req.body.Id){
                            t.push(preferredToUpdate[i]);
                        }
                    }
                    preferredToUpdate = t;
                }

            }
            Preferred.findOneAndUpdate({'Email' : userEmail}, {$set:{'Preferred': preferredToUpdate}}, {upsert:true}, (err, updated) => {
                if(err){
                    response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500);
                }else{
                    response = new RequestResult(types.SUCCESS, responseTitle, responseText, err, 200);
                }
                res.json(response);
            });
        }
    });
};