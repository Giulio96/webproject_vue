var mongoose = require('mongoose');
var PointFormModel = mongoose.model('formpointsinfos');
var PointImages = mongoose.model('pointsimages');
var PointTypes = mongoose.model('pointstypes');
var Point = mongoose.model('points');
var InterestPoints = mongoose.model('interestpoints');
var RequestResult = require("../../www/utilities/RequestResult");
var types = require("../../www/utilities/Types.js");
var PointImages = require('../models/pointImagesModel');
var reviewField = mongoose.model('formreviews')
var Reviews = mongoose.model('reviews')
var Confirm = mongoose.model('confirm')


exports.home = function(req, res) {
 /*   console.log("req.session.email in Home", req.session.Email);
    console.log("req.session.role: ", req.session.Role);

    let Table = req.session.Role === 1 ? User : Admin;
    //console.log("email: "+req.session.Email)
    Table.findOne({'Email': req.session.Email}, function(err, user){
        if (err) {
            res.send(err);
        } else {
            if (user === null) {
                var err = new Error('Not authorized! Go back!');
            //    res.sendFile(appRoot + '/www/pages/signin.html');
            } else {
                console.log("vai alla home di "+req.session.Email);
                //res.redirect('/home');
                res.sendFile(appRoot + '/www/pages/home.html');
            }
      }
    });    */
    res.sendFile(appRoot + '/www/pages/home.html');
},
    

exports.getPlaceFromId = async function (req, res) {
    let places = [];

    for (let i = 0; i < req.body.length; i++) {
        await InterestPoints.find({'_id':req.body[i]}, function(err, res) {
            if (err) {
                res.send(err);
            } else {
                places[i] = res[0];
            }
        })  
    }
    res.send(places);
}

exports.userAction = function (req, res){  
    var response
    Confirm.findOne({'Email' : req.session.Email}, (err, userPlaces) => {
        if(err){
            response = new RequestResult(types.ERROR, "Error!", "Something went wrong..", err, 500)
            res.send(response)
        }else{
            var places = []
            if(userPlaces == null){//da inserire nel db, ancora non è presente               
                places.push(req.query.place)
                response = types.SUCCESS
                
            }else{ //l'utente è gia in questa tabella                          
                places = userPlaces.Places
                var found = 'false'        
                for(let i = 0; i < places.length; i++){                   
                    if(places[i] === req.query.place){
                        found = 'true'
                        response = types.ERROR                     
                    }
                }                
                if(found == 'false'){
                    places.push(req.query.place)   
                }
            }
            Confirm.findOneAndUpdate({ 'Email': req.session.Email}, {$set: {'Places': places } }, {upsert:true}, (err, updated) => {
                if(err){
                    response = types.ERROR
                }
            })        
            res.send(response)            
        }   
    })
}

exports.list_specific_marker_reviews = function(req, res) {

    Reviews.find({'idPlace':req.body._id}, function(err, reviews){
        if (err) {
            res.send(err);
        }
        var yourReview = ''
        reviews.forEach(elem => {
            if (elem.Email == req.session.Email){
                yourReview = elem      
            }
        })
        var index =  reviews.indexOf(yourReview)
        if(index >= 0 ){
            reviews.splice(index , 1); //rimuovi 1 elemento dall'indice index
        }     
        var result = {
            userReview : yourReview,
            allReviews : reviews,    
        }
        res.json(result);
    })       
}

exports.average_reviews_rating = function(req, res){   
    Reviews.find({'Place': req.body.name, 'idPlace':req.body._id}, function(err, reviews){
        if (err) {
            res.send(err);
        }             
        var average = 0
        reviews.forEach(elem => {
            average = average + elem.Rating           
        })
        average = average / reviews.length
        res.json(average)     
    })    
}

exports.list_review_fields = function(req, res){
    reviewField.find({}, function(err, field){
        if(err){
            res.send(err)
        }
        res.json(field)
    })
}

exports.list_fields = function(req, res) {
    PointFormModel.find({}, function(err, field) {
        if (err) {
            res.send(err);
        }
        res.json(field);
    });
}

exports.list_points = function(req, res) {
    Point.find({}, function(err, point) {
        if (err) {
            res.send(err);
        }
        res.json(point);
    })
}

exports.logout = function(req, res){
    req.session = null;
    response = new RequestResult(types.SUCCESS, null, null, null, 200);
    res.send(response);
}

exports.check_log = function(req, res){
    let response;
    //console.log(req.session);
    if(req.session.Email === undefined){
        response = new RequestResult(types.ERROR, "Session undefined!", "No session has been created for this user", null, 500);
    } else { 
        let session = {}
        session['email'] = req.session.Email;
        session['role'] = req.session.Role;
        response = new RequestResult(types.SUCCESS, "Authenticated", "Authentication success with user ["+session['email']+"] (role: "+session['role']+")", session, 200);
    }
    res.send(response);
}

exports.list_point_types = function(req, res) {
    PointTypes.find({}, function(err, pointTypes) {
        if (err) {
            res.send(err);
        }
        res.json(pointTypes);
    })
}

exports.list_interest_points = function(req,res) {
    InterestPoints.find({}, function(err, points){
        if (err) {
            res.send(err);
        }
        res.json(points);
    })
}

exports.list_point_images = function(req, res) {
    var imageReq = new PointImages(req.query);
    PointImages.find({'name': imageReq.name, 'placeId': req.query.placeId }, function(err, images) {
        if (err) {
            res.send(err);
        }
        res.json(images);
    })
}

//only admin can add interest points directly effective
exports.add_interest_points_effective = function (req, res) {
    let point = req.body.point
    let images = req.body.images
    let response
    let new_point = new InterestPoints(point)
    new_point['effective'] = true
    new_point['accepted'] = 5
    new_point['email'] = req.session.Email
    
    new_point.save((err, new_point) => {
        if(err) {
            response = new RequestResult(types.ERROR, "Registration failed!", "Something went wrong during registration.", err, 500);
        } else {         
            let imageUploaded = 0
            let uploadFailed = 0
            //image managing stuff
            for (let i = 0; i < images.length; i++) {                
                var img = {
                    name: images[i].name,
                    placeId: new_point._id, //Fk
                    base64: images[i].base64
                }
                let image = new PointImages(img);
                //image saving
                image.save((err2, img) => {
                    if (err2) {
                        uploadFailed++;
                    } else {
                        imageUploaded++;
                    }
                });
            }
        }          
        response = new RequestResult(types.SUCCESS, "Point added succesfully!", "This point has been correctly added into the system", new_point, 200);      
        res.send({response : response, point : new_point});
    });
}

exports.add_interest_points = function(req, res) {    
    let point = req.body.point
    let images = req.body.images
    let response
    let new_point = new InterestPoints(point)
    new_point['effective'] = false
    new_point['accepted'] = 0
    new_point['email'] = req.session.Email
    
    new_point.save((err, new_point) => {
        if(err) {
            response = new RequestResult(types.ERROR, "Registration failed!", "Something went wrong during registration.", err, 500);
        } else {         
            let imageUploaded = 0
            let uploadFailed = 0
            //image managing stuff
            for (let i = 0; i < images.length; i++) {                
                var img = {
                    name: images[i].name,
                    placeId: new_point._id, //Fk
                    base64: images[i].base64
                }
                let image = new PointImages(img);
                //image saving
                image.save((err2, img) => {
                    if (err2) {
                        uploadFailed++;
                    } else {
                        imageUploaded++;
                    }
                });
            }
            //if You add a place you can't vote for it
            Confirm.findOne({'Email' : req.session.Email}, (err, userPlaces) => {
                if(err){
                    response = new RequestResult(types.ERROR, "Error!", "Something went wrong..", err, 500)
                    res.send(response)
                }else{
                    var places = []
                    if(userPlaces == null){//da inserire nel db, ancora non è presente                      
                        places.push(new_point._id.toString()) 
                        response = new RequestResult(types.SUCCESS, "Point added succesfully!", "This point has been correctly added into the system", new_point, 200);
                        
                    }else{ //l'utente è gia in questa tabella                          
                        //placeActions = JSON.parse(JSON.stringify(userActions.PlaceActions))
                        places = userPlaces.Places
                        places.push(new_point._id.toString())          
                        response = new RequestResult(types.SUCCESS, "Point added succesfully!", "This point has been correctly added into the system", new_point, 200);                                         
                    }                  
                    Confirm.findOneAndUpdate({ 'Email': req.session.Email}, {$set: {'Places': places }}, {upsert:true}, (err, updated) => {
                        if(err){
                            response = new RequestResult(types.ERROR, "Error!", "Something went wrong..", err, 500)
                        }
                    })
                }
            })
        response = new RequestResult(types.SUCCESS, "Point added succesfully!", "This point has been correctly added into the system", new_point, 200);
        }
        res.send({response : response, point : new_point});
    });
}

