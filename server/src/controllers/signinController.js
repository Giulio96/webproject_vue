var mongoose = require('mongoose');
var signinModel = mongoose.model('inputvalidations');
var User = mongoose.model('users');
var Admin = mongoose.model('admins');
var Role = mongoose.model('roles');
var crypto = require('crypto');
var RequestResult = require("../../www/utilities/RequestResult");
var types = require("../../www/utilities/Types.js");

exports.signin = function(req, res) {
    res.sendFile(appRoot + '/www/pages/signin.html');
};

exports.list_fields = function(req, res) {
    signinModel.find({}, function(err, field) {
        if (err) {
            res.send(err);
        }
        res.json(field);
    });
};

exports.get_role = function(req, res){
    let mail = req.params.mail
    var query = Role.findOne({ 'mail': mail });

    query.exec(function(err, role){
        if (err) {
            return res.send(err)
        }
        let response = new RequestResult();
        if(role == null){
            response.setTitle("User not registered").setMessage("This user has not been registered yet!").setType(types.ERROR);
        }else{
            response.setType(types.SUCCESS).setContent(role);
        }
        res.send(response);
    });
}

exports.set_user_role = function(req,res){
    let new_role = new Role(req.body);
    new_role.save((err, new_role)=>{
        if(err) {
            response = new RequestResult(types.ERROR, "Registration failed!", "Something went wrong during registration.", err, 500);
        } else {
            response = new RequestResult(types.SUCCESS, "Registration completed!", "You've been correctly added into the system.", err, 200);
        }
        res.send(response);
    });
}

exports.login = function(req, res){
    var userReq = new User(req.body);
    let Table = userReq.Role === 1 ? User : Admin;
    Table.findOne({'Email': userReq.Email}, function(err, user) {
        if(err) {
            let response = new RequestResult(types.ERROR, "An error occurred...", "Something went wrong in request", err, 500);
            res.send(response);
        } else {
            let response = new RequestResult(types.SUCCESS, "OK", "Success", user, 200)
            if(user == null ) {
                response.setTitle("Not registered").setMessage("You aren't registered into the system yet").setType(types.ERROR);
                res.send(response);
            } else {
                var password_digited_hashed = crypto.createHmac('sha512', user.Salt).update(req.body.Password).digest('hex');
                if (password_digited_hashed === user.Password) {
                    req.session.Email = user.Email;
                    req.session.Role = userReq.Role;
                    
                    response.setTitle("Login successful!").setMessage("Welcome "+user.Username).setContent(user);
                } else {
                    response.setTitle("Wrong credentials").setMessage("The password inserted is wrong").setType(types.ERROR);
                }
                res.send(response);
            }
        }
    });
}

exports.create_user = function(req, res) {
    user = req.body;
    delete user['Confirm password'];
    var new_user = new User(user);
    User.findOne({'Email': new_user.Email}, function(err, user){
        let response; 
        if(err) {
            response = new RequestResult(types.ERROR, "An error occurred...", "Something went wrong while parsing registered users.", err, 500);
            res.send(response);
        } else { 
            if(user === null) {
                salt_length = 32;
                var salt = crypto.randomBytes(Math.ceil(salt_length/2)).toString('hex').slice(0,salt_length);  
                new_user.Password = crypto.createHmac('sha512', salt).update(new_user.Password).digest('hex');
                new_user.Salt = salt;
                let a = new_user.save((err, user)=>{
                    if(err) {
                        response = new RequestResult(types.ERROR, "Registration failed!", "Something went wrong during registration.", err, 500);
                    } else {
                        response = new RequestResult(types.SUCCESS, "Registration completed!", "You've been correctly added into the system.", err, 200);
                    }
                    res.send(response);
                });
            } else {
                response = new RequestResult(types.ERROR, "Registration failed!", "This user is already registered in the system.", null, 500);
                res.send(response);
            }
        }
    });
}

exports.create_admin = function(req, res) {
    user = req.body
    delete user['Confirm password']
    delete user['Photo']
    var new_admin = new Admin(user);
    Admin.findOne({'Email': new_admin.Email}, function(err, resAdmin){
        let response; 
        if(err) {
            response = new RequestResult(types.ERROR, "An error occurred...", "Something went wrong while parsing registered admins.", err, 500);
            res.send(response);
        } else { 
            if(resAdmin === null) {
                salt_length = 32;
                var salt = crypto.randomBytes(Math.ceil(salt_length/2)).toString('hex').slice(0,salt_length);  
                new_admin.Password = crypto.createHmac('sha512', salt).update(new_admin.Password).digest('hex');
                new_admin.Salt = salt;
                let a = new_admin.save((err, admin)=>{
                    if(err) {
                        response = new RequestResult(types.ERROR, "Registration failed!", "Something went wrong during registration.", err, 500);
                    } else {
                        response = new RequestResult(types.SUCCESS, "Registration completed!", "You've been correctly added an admin into the system.", err, 200);
                    }
                    res.send(response);
                });
            } else {
                response = new RequestResult(types.ERROR, "Registration failed!", "This admin is already registered in the system.", null, 500);
                res.send(response);
            }
        }
    });
}

