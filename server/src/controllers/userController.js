var mongoose = require('mongoose')
var UserPlaces = mongoose.model('userplaces')
var UserScore = mongoose.model('userscores')
var RequestResult = require("../../www/utilities/RequestResult")
var types = require("../../www/utilities/Types.js")
var UserScore = mongoose.model('userscores')
var InterestPoints = mongoose.model('interestpoints')
var Confirm = mongoose.model('confirm')

var pointsForAcceptPlaces = 600
var pointsForReview = 300
var pointfForAddPlaces = 1000

exports.user_visited_places = function(req, res) {
    let userEmail = req.session.Email;
    UserPlaces.find({'Email' : userEmail}, function(err, places) {
        if (err) {
            res.send(err);
        }
        res.json(places);
    });
};

exports.set_visited_place = function(req, res){
    let newVisitedPlace = new UserPlaces();
    let userEmail = req.session.Email;
    
    UserPlaces.findOne({'Email' : userEmail}, function(err, userPlaces) {
        if (err) {
            res.send(err);
        } else { 
            let response = null;
           
            if(userPlaces === null) {           //nessun posto visitato
                placesToUpdate = req.body;      //questo è il primo che va salvato           
            
            } else {   //qualche posto l'ho già visitato, controllo se in questo ci sono già stato o lo aggiungo        
                for (let i = 0; i < userPlaces.Places.length; i++) {
                    if (userPlaces.Places[i] === req.body){ //posto già visitato
                        response = new RequestResult(types.SUCCESS, "Already saved!", "This point has already been saved!", err, 500);
                        res.json(response);
                        return;
                    }
                }            
                placesToUpdate = userPlaces.Places; 
                placesToUpdate.push(req.body);// posto non visitato, lo aggiungo a quelli visitati
            }    
            //memorizzo il nuovo posto tra quelli visitati
            UserPlaces.findOneAndUpdate({'Email' : userEmail}, {$set:{'Places': placesToUpdate}}, {upsert:true}, (err, updated) => {
                if(err) {
                    response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500);
                } else {
                    //guadagno punti per aver visitato un nuovo posto
                    UserScore.findOneAndUpdate({'Email' : userEmail}, { $inc: { 'TotReward' : 30} }, {upsert:true}, (err, updated) => {                
                        if (err) {
                            response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500)                        
                        } else {                   
                            response = new RequestResult(types.SUCCESS, "Congratulations!", "You visited new place! You earn +30 point experience", updated, 200)                        
                        }
                        res.send(response);
                    })                
                }     
            });
        }
    });
}

exports.check_can_make_review = function(req, res) {
    let response 
    UserPlaces.findOne({'Email' : req.session.Email}, (err, userPlaces) => {
        if (err) {
            response = new RequestResult(types.ERROR, "Error!", 500)
            res.send(response)
        } else { 
            if(userPlaces == null){
                response = new RequestResult(types.ERROR, "Error!","You have to visit the place before reviewing it.", 500)
                res.send(response)               
            } else {
                //controllo se ho già visitato questo posto
                let found = false
                console.log(userPlaces.Places.length);
                for (let i = 0; i < userPlaces.Places.length; i++) {
                    console.log(userPlaces.Places[i].idPlace);
                    console.log(req.body);
                    if (userPlaces.Places[i].idPlace === req.body._id) { //posto già visitato
                        found = true;
                    }
                }             
                if (found) {    //l'utente ha già visitato qusto posto, può recensirlo              
                    hasEnoughScore("review", req, res)                   
                } else {
                    response = new RequestResult(types.ERROR, "Error!","You have to visit the place before reviewing it.", 500)                      
                    res.send(response)
                }
            }
        }
    })    
    
}

exports.hasEnoughScoreToAddPlace = function(req, res) {
    hasEnoughScore("addPlace", req, res)
}

function hasEnoughScore(type, req, res){
    let score  
    let response    
    //se non hai abbastanza punti esperienza non puoi accettare un posto
    UserScore.findOne({'Email' : req.session.Email}, (err, userscore) => {                       
        if (err) {
            response = new RequestResult(types.ERROR, "Error!","You don't have enough point experience for doing a review. Visit more places", 500)
            res.send(response)
        }else{  //un utente può accettare un posto solo se ha almeno 300 punti
            if(userscore == null){
                score = 0
            }else{
                score = userscore.TotReward 
            }
                             
            if(type == "addPlace" ){
                limit = pointfForAddPlaces
            }else{
                limit = pointsForReview
            }            
            if(score >= limit){     //OK lo puoi fare                        
                response = new RequestResult(types.SUCCESS, 200);               
                res.send(response)
            }else{               //non hai abbastanza punti
                response = new RequestResult(types.ERROR, "Error!","You don't have enough point experience for this action. Visit more places", 500)                                  
                res.send(response)
            }
        }                     
    })       
}

exports.placeAccepted = function(req, res){
    let response
    let score
    //se non hai abbastanza esperienza non puoi accettare un posto
    UserScore.findOne({'Email' : req.session.Email}, (err, userscore) => {                
        if (err) {
            response = new RequestResult(types.ERROR, "Error!", "Something went wrong..", err, 500)                        
            res.send(response)
        }else{ 
            if (userscore == null){
                score = 0
            }else{      
                score = userscore.TotReward       
            }    
        }                   
    }).then( (response) => {
        //un utente può accettare un posto solo se ha almeno 600 punti
        if(score > pointsForAcceptPlaces){
    
        InterestPoints.find({'_id': req.body._id }, (err, interestPoint) => {
        if(err || interestPoint == "" || interestPoint == null){
                //console.log("interest point is undefined")
                if(err){
                    response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500)
                    res.send(response)
                }
                if(interestPoint == "" || interestPoint == null ){
                    response = new RequestResult(types.SUCCESS, "There is a problem", "Update your Map", 200)                        
                    res.send(response)
                }  
        }else{
            if(interestPoint[0]['effective']){
                response = new RequestResult(types.SUCCESS, "Update your map", "There's a new map configuration", 200)                        
                res.send(response)
            }else{
                effective_place = false
                //accetto che il posto esiste
                interestPoint[0]['accepted'] = interestPoint[0]['accepted'] + 1
                if(interestPoint[0]['accepted'] > 4){
                    interestPoint[0]['effective'] = true
                    effective_place = true
                    //chi ha proposto questo posto riceve i punti.
                    UserScore.findOneAndUpdate({'Email' : interestPoint[0]['email']}, { $inc: { 'TotReward' : 1000} }, {upsert:true}, (err, updated) => {                                      
                        //NOTIFICA A QUELL'UTENTE ((( ?? )))
                        /*if (err) {
                            response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500)                        
                        }else{                   
                            response = new RequestResult(types.SUCCESS, "Thanks!", "You confirmed a new place. Earned +50 point experience", updated, 200)                        
                        }
                        res.send(response)*/                      
                    })
                }else{
                    interestPoint[0]['effective'] = false
                }
                InterestPoints.findOneAndUpdate({'_id': req.body._id}, {$set: interestPoint[0]}, {$upsert:true}, (err, updated) => {
                    if(err){
                        response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500)
                        res.send(response)
                    }else{
                        //guadagno un punto per aver dato un feedback
                        UserScore.findOneAndUpdate({'Email' : req.session.Email}, { $inc: { 'TotReward' : 50} }, {upsert:true}, (err, updated) => {                
                            if (err) {
                                response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500)                        
                            }else{
                                if(effective_place == true){                            
                                    response = new RequestResult(types.SUCCESS, "Thanks!", "You confirmed a new place. Earned +50 point experience", "effective_place", 200)                        
                                }else{           
                                    response = new RequestResult(types.SUCCESS, "Thanks!", "You confirmed a new place. Earned +50 point experience", updated, 200)                        
                                }
                            }
                            res.send(response)                      
                        })    
                    }                   
                })
            }
        }
    })
    }else{//non ha abbastanza punti per fare accettare un posto, elimino l'azione

        Confirm.findOne({'Email' : req.session.Email}, (err, userPlaces) => {
        if(err){
            response = new RequestResult(types.ERROR, "Error!", "Something went wrong..", err, 500)
            res.send(response)
        }else{
            var places = [] //se l'utente non è nel db, places rimane vuoto
            if(userPlaces != null){                  
                //l'utente è gia in questa tabella e devo rimuovere questo places perchè non poteva fare l'azione                                    
                places = userPlaces.Places
                var index = places.indexOf(req.body._id)
                if (index !== -1){ places.splice(index, 1) }
            }
            Confirm.findOneAndUpdate({ 'Email': req.session.Email}, {$set: {'Places': places } }, {upsert:true}, (err, updated) => {
                if(err){                   
                    response = types.ERROR
                    res.send(response)
                }
            })            
        }   
        })
        response = new RequestResult(types.ERROR, "Error!","You don't have enough point experience for accept this place. Visit more places", 500)
        res.send(response)
    }
    })
}

exports.placeNotAccepted = function(req, res){
    let response
    let score
    //se non hai abbastanza punti esperienza non puoi accettare un posto
    UserScore.findOne({'Email' : req.session.Email}, (err, userscore) => {                
        if (err) {
            response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500)                        
            res.send(response)
        }else{ 
            if (userscore == null){
                score = 0
            }else{      
                score = userscore.TotReward       
            }    
        }                               
    }).then( (response) => {
        //un utente può accettare un posto solo se ha almeno 600 punti
        if(score > pointsForAcceptPlaces){
    
        InterestPoints.find({'_id': req.body._id }, (err, interestPoint) => {
            if(err || interestPoint == "" || interestPoint == null){
                //console.log("interest point is undefined")
                if(err){
                    response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500)
                    res.send(response)
                }
                if(interestPoint == "" || interestPoint == null){
                    response = new RequestResult(types.SUCCESS, "There is a problem", "Update your Map", 200)                        
                    res.send(response)
                }            
            }else{
                var place_to_delete = false
                if(interestPoint[0]['effective']){
                    response = new RequestResult(types.SUCCESS, "Update your map", "There's a new map configuration", 200)                        
                    res.send(response)
                }else{
                    interestPoint[0]['accepted'] = interestPoint[0]['accepted'] - 1
                    if(interestPoint[0]['accepted'] < - 4){
                        place_to_delete = true
                        //eliminare il posto perchè non esiste                
                        InterestPoints.deleteOne({'_id': req.body._id}).then()//response => {console.log(response)})
                        //dovrebbe riaggiornare tutti i campi
                    }else{
                        interestPoint[0]['effective'] = false
                    }
                    //se il campo non c'è, non viene creato, altrimenti il campo viene aggiornato
                    InterestPoints.findOneAndUpdate({'_id': req.body._id}, {$set: interestPoint[0]}, {$upsert:false}, (err, updated) => {
                        if(err){
                            response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500)
                            res.send(response)
                        }else{
                            //guadagno punti per avere dato un feedback 
                            UserScore.findOneAndUpdate({'Email' : req.session.Email}, { $inc: { 'TotReward' : 50} }, {upsert:true}, (err, updated) => {                
                                if (err) {
                                   response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500)                        
                                }else{   
                                    if(place_to_delete === true){         
                                        response = new RequestResult(types.SUCCESS, "Thanks!", "Your feedback is important. Earned +50 point experience", "place_to_delete", 200)                        
                                    }else{
                                        response = new RequestResult(types.SUCCESS, "Thanks!", "Your feedback is important. Earned +50 point experience", updated, 200)                        
                                    }
                                }
                                res.send(response)                      
                            })    
                        }                   
                    })
                }
            }
        }) 
    }else{//non ha abbastanza punti per fare accettare un posto, elimino l'azione

    Confirm.findOne({'Email' : req.session.Email}, (err, userPlaces) => {
    if(err){
        response = new RequestResult(types.ERROR, "Error!", "Something went wrong..", err, 500)
        res.send(response)
    }else{
        var places = [] //se l'utente non è nel db, places rimane vuoto
        if(userPlaces != null){           
            //l'utente è gia in questa tabella e devo rimuovere questo places perchè non poteva fare l'azione                                    
            places = userPlaces.Places
            var index = places.indexOf(req.body._id)
            if (index !== -1){ places.splice(index, 1) }
        }
        Confirm.findOneAndUpdate({ 'Email': req.session.Email}, {$set: {'Places': places } }, {upsert:true}, (err, updated) => {
            if(err){                   
                response = types.ERROR
                res.send(response)
            }
        })            
    }   
    })
    response = new RequestResult(types.ERROR, "Error!","You don't have enough point experience for accept this place. Visit more places", 500)
    res.send(response)
    }
})
}