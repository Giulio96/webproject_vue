var mongoose = require('mongoose');
var Challenge = mongoose.model('challenges');
var UserScores = mongoose.model('userscores');

var RequestResult = require("../../www/utilities/RequestResult");
var types = require("../../www/utilities/Types.js");

exports.save_new_challenge = function(req,res){
    let newChallenge  = new Challenge(req.body);
    newChallenge.Challenges = new Map(JSON.parse(req.body.Challenges));
    newChallenge.save((err, new_challenge)=>{
        if(err){
            response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500);
        }else{
            response = new RequestResult(types.SUCCESS, "Challenge added!", "You've been correctly added into the system.", new_challenge, 200);
        }
        res.send(response);
    });
};

exports.get_challenges = function(req, res){
    Challenge.find({}, function(err, challenges) {
        if (err) {
            res.send(err);
        }
        res.json(challenges);
    });
}

exports.get_challenge_achieved = function(req,res){
    UserScores.find({'Email' : req.session.Email}, function(err, challenges) {
        if (err) {
            res.send(err);
        }
        res.json(challenges);
    });
}

exports.set_challenge_achieved = function(req,res){
    let email = req.session.Email;
    UserScores.find({'Email' : email}, (err, score) => {
        if(err){
            response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500);
        } else {
            let reward;
            if (score == null){
                //console.log("CANCELLAMI TUTTO OK")
                reward = req.body.reward
            }
            //let reward = score.length === 0 ? req.body.reward : req.body.reward + score[0].TotReward;
            reward = req.body.reward + score[0].TotReward
            let challenges = [];
            if(score.length > 0){
                challenges = (Array.from(score[0].ChallengeAchieved));
            }
            challenges.push(req.body.id);
            UserScores.updateOne({'Email' : email}, {$set:{'Email': email, 'ChallengeAchieved': challenges, 'TotReward':reward}}, {upsert:true}, (err, updated) => {
                if(err){
                    response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500);
                }else{
                    response = new RequestResult(types.SUCCESS, "Congratulations!", "This challenge is completed!", err, 500);
                }
                res.json(response);
            });
        }
    });
}