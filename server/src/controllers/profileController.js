var mongoose = require('mongoose')
var Users = mongoose.model('users')
var UserPlaces = mongoose.model('userplaces')
var Reviews = mongoose.model('reviews')
//var Score = mongoose.model('scores')
var UserScore = mongoose.model('userscores')
var Admins = mongoose.model('admins')
var RequestResult = require("../../www/utilities/RequestResult")
var types = require("../../www/utilities/Types.js")
var crypto = require('crypto');

exports.profile = function(req, res) {
    res.sendFile(appRoot + '/www/pages/profile.html')
}

exports.places = function(req, res) {
    res.sendFile(appRoot + '/www/pages/userPlaces.html')
}

exports.reviews = function(req, res) {
    res.sendFile(appRoot+ '/www/pages/userReview.html')
}

exports.score = function(req, res) {
    res.sendFile(appRoot+ '/www/pages/userScore.html')
}

exports.review = function (req,res){  
    var review =  {
        Email : req.session.Email,
        Place : req.body.Place,   
        Title : req.body.Title,
        Description : req.body.Description,
        Rating : req.body.Rating,   
    }
    var response = ''
    
    Reviews.findOneAndUpdate({'Email' : req.session.Email, 'Place': req.body.Place, 'idPlace':req.body.idPlace}, {$set: review}, {upsert:true}, (err, updated) => {
        if(err){
            response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500)
            res.send(response);
        }else{
            if(!updated){   //guadagno di punti per aver recensito un nuovo posto          
                UserScore.findOneAndUpdate({'Email' : req.session.Email}, { $inc: { 'TotReward' : 50} }, {upsert:true}, (err, updated) => {                
                    if (err) {
                        response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500)                        
                    }else{                   
                        response = new RequestResult(types.SUCCESS, "Congratulations!", "Your review has been saved and you earned +50 point experience", updated, 200)                        
                    }
                    res.send(response);
                })                            
            }else{ //la recensione è stata aggiornata quindi non guadagno punti
                response = new RequestResult(types.SUCCESS, "Congratulations!", "Your review has been saved", updated, 200)
                res.send(response);
            }    
        }
    })
}

exports.profileScore = function(req, res) {
    var query = UserScore.findOne({'Email': req.session.Email })
    query.exec(function(err, score){
        if (err) {
            return res.send(err)
        }
        res.send(score)
    })
}

exports.profileReviews = function(req, res) {
    var query = Reviews.find({'Email': req.session.Email})
    query.exec(function(err, reviews){
        if (err) {
            return res.send(err)
        }
        res.send(reviews)
    })
}

exports.profileInfo = function(req, res) {
    var query = null;
    query = req.session.Role === 1 ?  Users.findOne({'Email': req.session.Email}) :  Admins.findOne({'Email': req.session.Email});
    query.exec(function(err, user){
        if (err) {
            return res.send(err)
        }
        res.send(user)
    })
}

exports.changeUserDetails = function(req, res){
    var table = req.session.Role === 1 ?  Users :  Admins;
    var query =  table.findOne({'Email': req.session.Email});
    let name = req.body.name;
    let pwd = req.body.pwd;
    let photo = req.body.photo;
    let oldPwd = req.body.oldPwd;
    let error = false;
    let u = null;
    let errorMsg = "Something went wrong during update...";
    table.findOne({'Email': req.session.Email}, function(err, user) {
        u = user;
        if(name != ''){
            table.findOneAndUpdate({'Email': req.session.Email}, {$set: {'Username' : name}}, {upsert: true}, (err, updated) => {
                error = err;
            });
        }
       
        if(photo != ''){
            table.findOneAndUpdate({'Email': req.session.Email}, {$set: {'Photo' : photo}}, {upsert: true}, (err, updated) => {
                error = err;
            });
        }
    
        if(pwd != ''){
            let password_digited_hashed = crypto.createHmac('sha512', u.Salt).update(oldPwd).digest('hex');
            if (password_digited_hashed === u.Password) {
                let salt_length = 32;
                let salt = crypto.randomBytes(Math.ceil(salt_length/2)).toString('hex').slice(0,salt_length);  
                let password = crypto.createHmac('sha512', salt).update(pwd).digest('hex');
                table.findOneAndUpdate({'Email': req.session.Email}, {$set: {'Password' : password}}, {upsert: true}, (err, updated) => {
                    error = err;
                });
                table.findOneAndUpdate({'Email': req.session.Email}, {$set: {'Salt' : salt}}, {upsert: true}, (err, updated) => {
                    error = err;
                });
            }else{
                error = true;
                errorMsg = "Old password is not correct, please try again!";
            }
           
        }
        let response = null;
        if(error) {
    
            response = new RequestResult(types.ERROR, "Error!", errorMsg, error, 500);
        } else {
            response = new RequestResult(types.SUCCESS, "Profile updated!", "Your profile has been updated", error, 200);
        }
        res.send(response);
    });

   
}

exports.profilePlaces = function(req, res) {
   //console.log("req.sess.email",req.session.Email)
    var query = UserPlaces.findOne({ 'Email': req.session.Email })
    query.exec(function(err, userPlaces){
        if (err) {
            return res.send(err)
        }
        res.send(userPlaces.Places)
    })
}

exports.user_photo = function(req, res) {
    var query = Users.findOne({'Email': req.session.Email}).select('Photo');
    query.exec(function(err, photo){
        if (err) {
            return res.send(err)
        }
        res.send(photo)
    })
}

exports.update_profile_image = function(req, res) {
    console.log("Chiamato l'update");
    //var photo = {'Photo' : req.body.Photo};
    Users.findOneAndUpdate({'Email': req.session.Email}, {$set: {'Photo' : req.body.Photo}}, {upsert: true}, (err, updated) => {
        if(err) {
            response = new RequestResult(types.ERROR, "Error!", "Something went wrong...", err, 500)
        } else {
            response = new RequestResult(types.SUCCESS, "Congratulations!", "Your profile's image has been updated", updated, 200)
        }
        res.send(response);
    });
}