module.exports = function(app) {

    var signinController = require('../controllers/signinController');
    var homeController = require('../controllers/homeController');
    var challengeController = require('../controllers/challengesController');
    var userController = require('../controllers/userController');
    var profileController = require('../controllers/profileController');
    var preferredController = require('../controllers/preferredController');

    app.route('/api/findPlace')
        .post(homeController.getPlaceFromId);

    app.route('/api/logout')
        .post(homeController.logout);

    app.route('/api/check_log')
        .get(homeController.check_log)

    app.route('/api/review')
        .post(profileController.review)

    app.route('/api/updateProfileImage')
        .post(profileController.update_profile_image)

    app.route('/api/listSpecificMarkerReviews')
        .post(homeController.list_specific_marker_reviews)
    
    app.route('/api/averageReviewsRating')
        .post(homeController.average_reviews_rating)
        
    app.route('/api/pointImages')
        .get(homeController.list_point_images)

    app.route('/api/interestPoints')
        .get(homeController.list_interest_points)
        .post(homeController.add_interest_points)

    app.route('/api/interestPointsEffective')
        .post(homeController.add_interest_points_effective)

    
    app.route('/api/fields')
        .get(signinController.list_fields)
        .post(signinController.create_user)
    
    app.route('/api/newAdmin')
        .post(signinController.create_admin)

    app.route('/api/roles')
        .post(signinController.set_user_role)
        
    app.route('/api/roles/:mail')
        .get(signinController.get_role)

    app.route('/api/login')
        .post(signinController.login)
    
    app.route('/api/placeAccepted')
        .post(userController.placeAccepted)
    
    app.route('/api/placeNotAccepted')
        .post(userController.placeNotAccepted)

    app.route('/api/pointFields')
        .get(homeController.list_fields)

    app.route('/api/pointTypes')
        .get(homeController.list_point_types)
    
    app.route('/api/reviewFields')
        .get(homeController.list_review_fields)
    
    app.route('/api/checkCanMakeReview')
        .post(userController.check_can_make_review)
    
    app.route('/api/hasEnoughScoreToAddPlace')
        .get(userController.hasEnoughScoreToAddPlace)
    
    app.route('/api/userAction')
        .get(homeController.userAction)

    // CHALLENGES ROUTES    
    app.route('/api/challenges')
        .post(challengeController.save_new_challenge)
        .get(challengeController.get_challenges)

    app.route('/api/challenge_achieved')
        .get(challengeController.get_challenge_achieved)
        .post(challengeController.set_challenge_achieved)
    
    //USER ROUTES
    app.route('/api/visited_places')
        .post(userController.set_visited_place)
    
    app.route('/api/change-user-details')
        .post(profileController.changeUserDetails);

    app.route('/api/profile_info')
        .get(profileController.profileInfo)

    //PREFERRED ROUTES
    app.route('/api/favourites')
        .get(preferredController.get_preferred_of_user)
        .post(preferredController.toggle_preferred)
/*
    app.route('/userPlaces')
        .get(profileController.places)
    
    app.route('/userReviews')
        .get(profileController.reviews)

    app.route('/userScore')
        .get(profileController.score)
    */
    app.route('/api/userScore')
        .get(profileController.profileScore)
    
    app.route('/api/userReviews')
        .get(profileController.profileReviews)

    app.route('/api/userInfo')
        .get(profileController.profileInfo)
       
    app.route('/api/userPhoto')
        .get(profileController.user_photo)

    app.route('/api/userPlaces')
        .get(userController.user_visited_places)

    app.use(function(req, res, next){ 
        res.setHeader("Content-Type", "text/plain"); 
        res.status(404).send("Ops... Pagina non trovata"); 
    });
};