var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AdminSchema = new Schema({
    Username: {
        type: String
    },
    Email: {
        type: String
    },
    Password: {
        type: String
    },
    Birthdate: {
        type: String
    },
    Sex: {
        type: String
    },
    Salt: {
        type: String
    },
    
    Photo: {
        type: String
    },
    Role:{
        type: Number
    }
    
});

module.exports = mongoose.model('admins', AdminSchema);