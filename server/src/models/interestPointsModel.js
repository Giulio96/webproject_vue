var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var InterestPointSchema = new Schema({
    name: {
        type: String
    },
    description: {
        type: String
    },
    type: {
        type:String
    },
    subtype:{
        type: String
    },
    latitude:{
        type: String
    },
    longitude:{
        type: String
    },
    effective:{
        type: Boolean
    },
    accepted:{
        type: Number
    },
    email:{
        type: String
    },
    key: {
        type: String
    }
});

module.exports = mongoose.model('interestpoints', InterestPointSchema);