var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PointTypeSchema = new Schema({
    Type: {
        type: String
    },
    Category: {
        type: String
    },
    Name: {
        type: String
    },
    Reward: {
        type: Number
    }
});

module.exports = mongoose.model('pointstypes', PointTypeSchema);