var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserPlacesSchema = new Schema({
    Email: {
        type: String
    },

    Places: {
        type: []
    }
});

module.exports = mongoose.model('userplaces', UserPlacesSchema);