var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserPreferredSchema = new Schema({
    Email: {
        type: String
    },

    Preferred: {
        type: []
    }
});

module.exports = mongoose.model('userfavourites', UserPreferredSchema);