var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    Username: {
        type: String
    },
    Email: {
        type: String
    },
    Password: {
        type: String
    },
    Birthdate: {
        type: String
    },
    Sex: {
        type: String
    },
    Photo: {
        type: String
    },
    Salt: {
        type: String
    },
    Role:{
        type: Number
    }
    
});

module.exports = mongoose.model('users', UserSchema);