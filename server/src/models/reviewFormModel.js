var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReviewFieldSchema = new Schema({
    Name: {
        type: String
    },
    Type: {
        type: String
    }
});

module.exports = mongoose.model('formreviews', ReviewFieldSchema);