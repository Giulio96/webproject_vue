var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FormPointSchema = new Schema({
    name: {
        type: String
    },
    options: {
        type: String
    }
});

module.exports = mongoose.model('formpointsinfos', FormPointSchema);