var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ConfirmSchema = new Schema({
    Email: {
        type: String
    },

    Places:{
        type : []
    }
    
});

module.exports = mongoose.model('confirm', ConfirmSchema);