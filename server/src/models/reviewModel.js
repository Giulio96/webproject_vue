var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReviewsSchema= new Schema({
    Email : {
        type: String
    },

    Place: {
        type: String
    },

    Title: {
        type: String
    },

    Description: {
        type: String
    },

    Rating: {
        type: Number
    },

    idPlace: {
        type: String
    }
});

module.exports = mongoose.model('reviews', ReviewsSchema);