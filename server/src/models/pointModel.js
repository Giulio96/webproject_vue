var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PointSchema = new Schema({
    Name: {
        type: String
    },
    Description: {
        type: String
    },
    Type: {
        type: String
    }

});

module.exports = mongoose.model('points', PointSchema);