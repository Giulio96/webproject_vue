var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ChallengeSchema = new Schema ({

    Name:{
        type: String
    },

    Reward: {
        type: Number
    },

    Challenges: {
        type: Map,
        of: Number
    }
});

module.exports = mongoose.model('challenges', ChallengeSchema);