var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ScoreSchema = new Schema({
    
    Park: {
        type: String
    },

    Museum: {
        type: Number
    },

    Monument: {
        type: Number
    },

    Email: {
        type: String
    }

});

module.exports = mongoose.model('scores', ScoreSchema);