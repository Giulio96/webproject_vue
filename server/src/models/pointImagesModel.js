var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ImageSchema = new Schema({
    name: {
        type: String
    },
    placeId: {
        type: String
    },
    base64: {
        type: String
    }
});

module.exports = mongoose.model('pointsimages', ImageSchema);