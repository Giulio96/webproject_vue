var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RoleSchema = new Schema({
    mail: {
        type: String
    },
    role: {
        type: Number
    }
});

module.exports = mongoose.model('roles', RoleSchema);