var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SigninSchema = new Schema({
    name: {
        type: String
    },
    minsize: {
        type: Number
    },
    maxsize: {
        type: Number
    },
    regularexp: {
        type: String
    },
    mandatory: {
        type: Boolean
    },
    placeholder: {
        type: String
    },
    validationerror: {
        type: String
    },
    minage: {
        type: Number
    },
    maxage: {
        type: Number
    },
    options: {
        type: String
    },
    default: {
        type: String
    },
    matchwith: {
        type: String
    }
});

module.exports = mongoose.model('inputvalidations', SigninSchema);