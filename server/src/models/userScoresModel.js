var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserScoresSchema = new Schema({
    Email: {
        type: String
    },

    ChallengeAchieved: {
        type: []
    },

    TotReward:{
        type: Number
    }
});

module.exports = mongoose.model('userscores', UserScoresSchema);