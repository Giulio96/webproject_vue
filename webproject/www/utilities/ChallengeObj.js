// used for convenience after getting challenges from server
export class ChallengeObj{
    constructor(id, name, reward, challenges){
        this.id = id;
        this.name = name;
        this.reward = reward;
        this.challenges = challenges;
    }
}