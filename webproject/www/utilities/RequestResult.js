class RequestResult{
    constructor(type, title, message, content, code){
        this.type = type;
        this.title = title;
        this.message = message;
        this.content = content;
        this.code = code;
    }

    setType(type){
        this.type = type;
        return this;
    }

    setTitle(title){
        this.title = title;
        return this;

    }
    
    setMessage(message){
        this.message = message;
        return this;

    }

    setCode(code){
        this.code = code;
        return this;

    }

    setContent(content){
        this.content = content;
    }
    getType(){
        return this.type;
    }

    getTitle(){
        return this.title;
    }

    getMessage(){
        return this.message;
    }

    getContent(){
        return this.content;
    }

    getCode(){
        return this.code;
    }

    print(){
        console.log("RESPONSE: \n Type: "+this.type+"\n Title: "+this.title+"\n Message: "+this.message+"\nContent: "+this.content+"\nCode:"+this.code);
    }
}

module.exports = RequestResult;