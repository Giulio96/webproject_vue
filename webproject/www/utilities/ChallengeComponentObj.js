/*
    Single challenge component, if all components are completed the user has achieved the entire challenge
*/

export class ChallengeComponentObj{
    constructor(type, name, toComplete, alreadyCompleted){
        this.type = type;
        this.name = name;
        this.toComplete = toComplete;
        this.alreadyCompleted = alreadyCompleted;
    }

     isCompleted(){
        return this.toComplete === this.alreadyCompleted;
    }
}