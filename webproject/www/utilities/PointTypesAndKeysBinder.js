export class PointTypesAndKeysBinder {

    constructor(){
        this.keyNameMap = new Map();
        this.keyNameMap.set( "cat", "Church/Cathedral");
        this.keyNameMap.set( "cas" ,"Castle");
        this.keyNameMap.set( "arc", "Archeological Site");
        this.keyNameMap.set( "his", "Historic site");
        this.keyNameMap.set( "mon", "Monument");
        this.keyNameMap.set( "mus", "Museum");
        this.keyNameMap.set( "par", "Park");
        this.keyNameMap.set( "sta", "Statue");
        this.nameKeyArray =  Array.from(this.keyNameMap.entries());
    }

    getSuperkeyFromKey(key){
        if(key === 'sta'){
            return 'mon';
        }
        return "";
    }

    getKeyByName(name){
        if(name === 'Church/Cathedral'){
            return 'cat';
        }
        for(let i = 0; i < this.nameKeyArray.length; i++){
            if(this.nameKeyArray[i][1] === name){
                return this.nameKeyArray[i][0];
            }
        }
    }

    getNameByKey(key){
        return this.keyNameMap.get(key);
    }
}