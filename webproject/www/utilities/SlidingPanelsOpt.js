export const SlidingOpt = {
    USER_PROFILE: "user_profile",
    USER_NAV_OPT: "user_nav_opt",
    MARKER_FILTERS: "marker_filters",
    LISTED_PLACES: "listed_places",
    USER_REVIEWS: "user_reviews",
    USER_PLACES: "user_places",
    CREATE_CHALLENGE: "create_challenge"
}