import axios from 'axios';
import {ipServer} from "../../src/consts"

var myPlaces = new Array();
var myUserReviews = new Map();

export class ReviewManager{

    static async initialize(){
        await this.getPlacesList();
        await this.getPlacesReviews();

    }

    static async getPlacesList(){
        return new Promise((resolve, reject) => {
            axios.get("http://" + ipServer + ":3000/api/interestPoints")
                .then(response => {
                    for(let i = 0; i < response.data.length; i++){
                        myPlaces.push(response.data[i]);
                    }
                    resolve();
                });
        });
    }

    static async getPlacesReviews(){
        for(let i = 0; i < myPlaces.length; i++){
            let obj = {};
            obj['_id'] = myPlaces[i]._id;
            let p = new Promise((resolve, reject) => {
                axios.post("http://" + ipServer + ":3000/api/listSpecificMarkerReviews", obj)
                    .then(response => {
                        //this.userReview = response.data.userReview
                        //this.markerReviews = response.data.allReviews
                        myUserReviews.set(obj._id, response.data.userReview);
                        resolve()
                    })  
            })
            await p;            
        }

        console.log(myUserReviews);
    }

    static GetReviewOfPlace(id){
        return myUserReviews.get(id);
    }

    static updateReviewOfPlace(id, review){
        myUserReviews.set(id, review);
    }

}