/*
    contains all the references (ChallengeComponent) about a single challenge of a user
*/

export class ChallengeProgressObj{
    constructor(id, name, reward, challenges){

        //array of ChallengeComponent
        this.id = id;
        this.name = name;
        this.reward = reward;
        this.challenges = challenges;
    }


    //if at least one challenge component is not completed, the challenge is still not achieved
    isAchieved(){
        for(let i = 0; i < this.challenges.length; i++){
            if(!this.challenges[i].isCompleted()){
                return false;
            }
        }
        return true;
    }
}