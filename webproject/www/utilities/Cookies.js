export const Cookies  = {
    USERNAME: "username",
    ROLE: "role",
    EMAIL: "email",
    SEX: "sex",
    BIRTHDATE: "birthdate",
    FILTERS: "filters",
}
