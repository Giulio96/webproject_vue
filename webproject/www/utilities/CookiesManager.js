var Cookies = require('../utilities/Cookies');
var VueCookies = require('vue-cookies');

export class CookiesManager {

    deleteCookie(name) {
        VueCookies.remove(name);
    }

    writeCookie(name, value) {
        VueCookies.set(name, value);
    }

    readCookie(name) {
        return VueCookies.get(name);
    }

    deleteAll(){
        this.deleteCookie(Cookies.Cookies.USERNAME)
        this.deleteCookie(Cookies.Cookies.ROLE)
        this.deleteCookie(Cookies.Cookies.SEX)
        this.deleteCookie(Cookies.Cookies.EMAIL)
    }

}
