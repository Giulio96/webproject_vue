export class  IconObj{
    constructor(name, title, path){
        this.path = path;
        this.name = name;
        this.title = title;
    }
}