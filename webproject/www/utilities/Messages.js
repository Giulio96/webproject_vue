export const MsgDictionary = {

    NAVIGATION_INFO : 'navigation_info',
    RESET_PATH: "reset_path",
    CLOSE_NAVIGATION: 'close_navigation',
    CLOSE_NAVIGATION_PANEL: 'close_navigation_panel',
    ACTIVE_NAVIGATION: 'active_navigation',
    
    CLOSE_ALL_PANELS: "close_all_panels",

    REVIEW_PLACE: "review_place",

    // MESSAGE SENT WHEN ALL IS LOADED
    DATA_LOADED: "has_data_been_loaded",

    // MESSAGE FOR CLOSING MARKER'S LITTLE PANEL
    CLOSE_MARKER_PANEL: "close_marker_panel",

    // SET NEW VISITED PLACE
    NEW_VISITED: "new_visited",

    //SET NEW FAVOURITE PLACE
    NEW_FAVOURITE: "new_favourite",

    // INFO ABOUT MARKER REQUEST
    MARKER_INFO_REQUEST: "marker_info_request",

    // OPENS/CLOSES SLIDING PANEL WITH MARKER'S INFO
    OPEN_MARKER_SLIDING: "open_marker_sliding",
    CLOSE_MARKER_SLIDING: "close_marker_sliding",

    // MESSAGE FOR RESET MARKER INFO PANEL
    PANEL_RESET: "reset_panel",

    //MARKER'S PANEL ASK PATH TO MAP
    ASK_PATH: "ask_path",

    ADD_ADMIN: "add_admin",
    
    //THE USER IS CLOSE ENOUGH AT THE TARGET
    OK_YOU_ARE_CLOSE: "ok_close",

    //THE USER IS TOO FAR FROM THE TARGET
    NO_TOO_FAR: "no_too_far",

    //MESSAGE FOR ASKING IF THE USER IS ENOUGH CLOSE AT THE TARGET TO INTERACT WITH
    CLOSE_ENOUGH_TO_INTERACT: "close_enough_to_interact",

    //MESSAGE THAT STARTS NAVIGATION
    START_NAVIGATION: "start_navigation",

    //MESSAGE THAT SHOWS PATH ON MAP
    SHOW_PATH: "show_path",

    //MARKERS FROM MAP LOADED
    MARKERS_READY: "markers_ready",

    //MESSAGE FROM MAP LOADED
    MAP_READY: "map_ready",

    //MESSAGE THAT ASK POSITION FOR CALCULATE DISTANCE
    POSITION_REQUEST: "position_request",

    //MESSAGE FOR SENDING ACTUAL POSITION
    POSITION_SENDING: "position_sending",

    //MESSAGE FOR PLACE CLICKED IN LIST OF PLACES
    PLACE_CLICKED: "place_clicked",

    //MESSAGE FOR NEW IMAGE IN CAROUSEL
    CAROUSEL_ADD_IMAGE: "image_added",

    //MESSAGE FOR IMAGE RECEIVED
    IMAGE_LOADED: "image_loaded",

    //MESSAGE FOR CREATE A NEW REVIEW
    CREATE_REVIEW: "create_review",
    
    //MESSAGE FOR BE REDIRECTED TO THE MAP CLOSE THE MARKER SELECTED
    SHOW_PLACE_ON_MAP: "show_place_on_map",

    //UPDATEd_REVIEW: "updated_review",
    UPDATED_REVIEW: "updated_review",
 
    //MESSAGES FOR CENTER MAP
    CENTER_MAP : "center_map",

    //MESSAGES FOR COORDINATES FROM ANDRIOD
    COORDINATES : "coordinates",

    //MESSAGES FOR OPEN SLIDING PANEL
    OPEN_SLIDING_PANEL: "open_panel",
    CLOSE_SLIDING_PANEL : "hide_sliding",

    //MESSAGES FOR PROFILE PANEL COMPONENTS
    SHOW_USER_PROFILE_PANEL : "on_show_user_profile_panel",
    HIDE_USER_PANELS : "on_hide_profile_panels",

    //MESSAGES FOR HOME COMPONENT
    GLOBAL_SHOW_SPINNER : "global_show_spinner",

    //MESSAGES FOR CAROUSEL COMPONENT
    CAROUSEL_INIT_IMAGES : 'init_images_for_carousel',

    //MESSAGES FOR SPINNER COMPONENT
    SHOW_SPINNER: "spinner_on_or_off",

    //MESSAGES FOR MAP COMPONENT
    ADD_MARKER: "add_marker_clicked",
    ON_MARKER_ADDED: "on_marker_added",
    SHOW_MAP: "show_map",
    HIDE_MAP: "hide_map",

    //MESSAGES FOR MARKER INFO PANEL (OPEN)
    ON_MARKER_SELECTED: 'on_marker_panel_loaded',

    //MESSAGES FOR MARKER DETAILS PANEL (EDIT)
    MAP_NEW_MARKER: 'on_marker_insert',

    //MESSAGES FOR CHALLENGE DETAILS PANEL (EDIT)
    SHOW_EDIT_CHALLENGE: 'on_new_challenge',
    HIDE_EDIT_CHALLENGE: 'hide_challenge_panel',

    MAP_NEW_MARKER: 'on_marker_insert',

    MAP_NEW_MARKER_ADMIN: 'marker_insert_by_admin',

    //MESSAGES FOR FILTER TYPE IN MAP
    FILTER_ON_TYPE: 'filter_on_type',
    CHANGE_FAVOURITE: 'set_or_unset_favourite'


}