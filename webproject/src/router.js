import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "login",
      component: () =>
        import("./views/Login.vue")
    },
    
    {
      path: "/signin",
      name: "signin",

      component: () =>
        import( "./views/Signin.vue")
    },

    {
      path: "/home",
      name: "home",
      
      component: () =>
        import( "./views/Home.vue")
    },

    {
      path: '/:device', 
      
      component: () => 
        import( "./views/Home.vue")
    },

    {
      path: '/login/:device', 
      
      component: () => 
        import( "./views/Login.vue")
    },
  ]
});
