import App from "./App.vue";
import router from "./router";
import store from "./store";
import BootstrapVue from 'bootstrap-vue'
import axios from 'axios';
import VueAxios from 'vue-axios';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import * as VeeValidate from 'vee-validate';
import * as VueGoogleMaps from 'vue2-google-maps';
import Vue from "vue";


axios.defaults.withCredentials = true

Vue.use(VueAxios, axios);

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDaBm_6SCVxrbjCgi4HZJM18a3rfH65nlg',
    libraries: 'places,drawing,visualization'
  },
  installComponents: true
});

Vue.use(VeeValidate, {
  // This is the default
  inject: true,
  // Important to name this something other than 'fields'
  fieldsBagName: 'veeFields',
  // This is not required but avoids possible naming conflicts
  errorBagName: 'veeErrors'
})

Vue.use(BootstrapVue)
Vue.use(require('vue-cookies'))

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
