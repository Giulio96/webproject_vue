import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {}
});

export const store = Vue.observable({
  isNavOpen: false,
  isProfileOpen: false,
  isPlacesOpen: false,
})

export const mutations = {

  setIsNavOpen(value) {
    store.isNavOpen = value
  },

  setIsProfileOpen(value){
    store.isProfileOpen = value
  },

  setIsPlacesOpen(value){
    store.isPlacesOpen = value
  },
  
  toggleNav(){
    store.isNavOpen = !store.isNavOpen
    
    if(!store.isNavOpen){ //chiudo slide nav
      //this.$root.$emit(MsgDictionary.SHOW_MAP);
      if(store.isProfileOpen){
        this.setIsProfileOpen(false) //hide profilo
        
      }

      if(store.isPlacesOpen){
        this.setIsPlacesOpen(false) //hide places
      }
    
    }
  },

  
}
