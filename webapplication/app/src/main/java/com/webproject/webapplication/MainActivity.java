package com.webproject.webapplication;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import static com.webproject.webapplication.Consts.MY_PERMISSIONS_REQUEST_COARSE_LOCATION;
import static com.webproject.webapplication.Consts.MY_PERMISSIONS_REQUEST_FINE_LOCATION;

public class MainActivity extends Activity {

    private Gps gps;
    private GpsObserver gObs;
    private WebView webView;

    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = webView.getSettings();
        if (0 != (getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE)) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        webSettings.setJavaScriptEnabled(true);
        webView.loadUrl(Consts.APP_URL);

       webView.addJavascriptInterface(new OpenCamera(), "camera");
       webView.addJavascriptInterface(this, "context");


        // valutare se bisogna inserire il controllo sull'attuale versione di android
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_FINE_LOCATION);
            }
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSIONS_REQUEST_COARSE_LOCATION);
            }
        } else {
            startGps();
        }

        webView.addJavascriptInterface(new WebAppInterface(this), "Android");
    }


    @JavascriptInterface
    public void openGallery() {
        Log.i("WEB_APP", "OPEN GALLERY FROM SITE");
        showToast("Provo ad aprire la galleria");
       // Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//

        try{
            //((Activity)mContext).startActivityForResult(gallery, 1);
            startActivityForResult(Intent.createChooser(intent, "Select File"),1);

        }catch(Exception e ){
            Log.i("WEB_APP", e.toString());
        }

       /* Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);*/

       // mContext.startActivity(intent);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Consts.MY_PERMISSIONS_REQUEST_COARSE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.e("TEST", "COARSE granted");
                    startGps();
                } else {
                    Log.e("TEST", "COARSE REFUSED");
                }
                return;
            }

            case Consts.MY_PERMISSIONS_REQUEST_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Log.e("TEST", "FINE granted");
                    startGps();
                } else {
                    Log.e("TEST", "FINE REFUSED");
                }
                return;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i("WEB_APP", "ON IMAGE PICKED ACTIVITY RESULT");

       /* super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageUri = data.getData();
            System.out.println(imageUri);
        }*/
        if (resultCode == Activity.RESULT_OK){
            if(requestCode == 1){
                try{

                    Uri imageUri = data.getData();
                    InputStream imageStream = getContentResolver().openInputStream(imageUri);
                    Bitmap b = BitmapFactory.decodeStream(imageStream);
                    toBase64(b);
                }catch(Exception e ){}
            }else{

                Bitmap bmp = (Bitmap) data.getExtras().get("data");
                toBase64(bmp);
            }
        }

    }


    private String toBase64(Bitmap b){

        try{

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            b.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] ba = baos.toByteArray();
            String imageEncoded = Base64.encodeToString(ba,Base64.DEFAULT);
            webView.loadUrl("javascript:onImageLoaded('"+imageEncoded+"')");

        }catch (Exception e ){
            Log.i("WEB_APP", e.toString());
        }

        return "";
       // return imageEncoded;
    }

    @SuppressLint("JavascriptInterface")
    private void startGps() {
        gObs = new GpsObserver(this, webView);
        gps = new Gps(this);
        gps.addObserver(gObs);
        webView.addJavascriptInterface(gObs, "AndroidBridge");

        gps.start();
    }

    /*public void openGallery(){
        Intent intent=new Intent(Intent.ACTION_PICK);
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.setType("image/*");
        //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
        // Launching the Intent
        startActivityForResult(intent, 1);
    }*/

    public void showToast(String toast) {
        Toast.makeText(this, toast, Toast.LENGTH_SHORT).show();
    }
}

