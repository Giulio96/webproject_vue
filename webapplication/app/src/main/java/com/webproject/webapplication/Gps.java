package com.webproject.webapplication;
import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

//Observable
public class Gps implements LocationListener {

    private final LocationManager locationManager;
    private List<GpsObserver> observers;
    private static double latitude;
    private static double longitude;
    private Context context;

    public Gps(Context c) {
        this.context = c;
        observers = new ArrayList<>();
        locationManager = (LocationManager)c.getSystemService(Context.LOCATION_SERVICE);
    }

    @Override
    public void onLocationChanged(Location loc) {
        latitude = loc.getLatitude();
        longitude = loc.getLongitude();
        for (GpsObserver obs: this.getObservers()) {
            obs.onCoordinatesChanged(latitude, longitude);
        }
    }

    @Override
    public void onProviderDisabled(String provider) { }

    @Override
    public void onProviderEnabled(String provider) { }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub
    }

    public void addObserver(GpsObserver g){
        this.observers.add(g);
    }

    public List<GpsObserver> getObservers() {
        return this.observers;
    }

    @SuppressLint("MissingPermission")
    public void start() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
    }
}