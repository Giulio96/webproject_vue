package com.webproject.webapplication;

public interface IGpsObserver {
    void onCoordinatesChanged(double lat, double lon);
}