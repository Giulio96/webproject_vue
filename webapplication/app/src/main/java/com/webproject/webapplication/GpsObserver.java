package com.webproject.webapplication;

import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Toast;

public class GpsObserver implements  IGpsObserver {

    Context mContext;
    WebView wb;

    /** Instantiate the interface and set the context */
    GpsObserver(Context c, WebView w) {
        mContext = c;
        wb = w;
    }

    @Override
    public void onCoordinatesChanged(double lat, double lon) {
        Log.e("TEST", "GPS Observer: "+lat+", "+lon);
        wb.loadUrl("javascript:onCoordinatesChanged("+lat+","+lon+")");
    }

}
