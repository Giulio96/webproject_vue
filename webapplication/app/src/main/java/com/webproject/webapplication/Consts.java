package com.webproject.webapplication;

public final class Consts {

    public static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 1;
    public static final int MY_PERMISSIONS_REQUEST_COARSE_LOCATION = 2;

    public static final String APP_URL = "http://192.168.5.16:8080?device=android";
    //public static final String APP_URL = "http://192.168.5.17:8080";

}
