package com.webproject.webapplication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class WebAppInterface extends AppCompatActivity {
    Context mContext;
    private static final int PICK_IMAGE = 100;
    private MainActivity main;

    Uri imageUri;

    /** Instantiate the interface and set the context */
    WebAppInterface(Context c) {
        this.main = new MainActivity();
        mContext = c;
    }


    @JavascriptInterface
    public void choosePic(){
        final CharSequence[] items = { "Take Photo", "Open Gallery",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
               // boolean result= Utility.checkPermission(TestFragment.this);
                String userChoosenTask = "";

                if (items[item].equals("Take Photo")) {
                    cameraIntent();

                } else if (items[item].equals("Open Gallery")) {
                    openGallery();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    //@JavascriptInterface
    public void openGallery() {
        Log.i("WEB_APP", "OPEN GALLERY FROM SITE");
        showToast("Provo ad aprire la galleria");
       // Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//

        try{
            //((Activity)mContext).startActivityForResult(gallery, 1);
            ((Activity)mContext).startActivityForResult(Intent.createChooser(intent, "Select File"),1);

        }catch(Exception e ){
            Log.i("WEB_APP", e.toString());
        }

       /* Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);*/

       // mContext.startActivity(intent);

    }

    private void cameraIntent()
    {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try{
            //((Activity)mContext).startActivityForResult(gallery, 1);
            ((Activity)mContext).startActivityForResult(intent,2);

        }catch(Exception e ){
            Log.i("WEB_APP", e.toString());
        }
    }



    /** Show a toast from the web page */
    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }

}