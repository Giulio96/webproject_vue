package com.webproject.webapplication;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.JavascriptInterface;

public class OpenCamera extends AppCompatActivity {

    @JavascriptInterface
    public void startCameraActivity(Context con) {
        Log.i("WEB_APP", "OPEN GALLERY");
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
        startActivityForResult(intent, 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("WEB_APP", "ON IMAGE PICKED ACTIVITY RESULT");

        super.onActivityResult(requestCode, resultCode, data);
       /* if (resultCode == RESULT_OK && requestCode == 1) {
            imageUri = data.getData();
            System.out.println(imageUri);
        }
*/
        if (resultCode == Activity.RESULT_OK)
            switch (requestCode){
                case 1:
                    //data.getData returns the content URI for the selected Image
                    Uri selectedImage = data.getData();
                    // imageView.setImageURI(selectedImage);
                    break;
            }
    }
}

